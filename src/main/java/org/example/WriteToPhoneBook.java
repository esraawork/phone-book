package org.example;

import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class WriteToPhoneBook {
    private static final JSONObject jsonObject = new JSONObject();
    public static void addEntry (String key, String value)
    {
        try
        {
            FileWriter jsonDataFile = new FileWriter("phoneBookData.json");
            jsonObject.put(key,value);
            jsonDataFile.write(jsonObject.toJSONString());
            jsonDataFile.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}

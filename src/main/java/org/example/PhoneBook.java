package org.example;

import org.json.simple.parser.ParseException;

import java.io.IOException;

public class PhoneBook
{
    public void addNewPhone(String name, String phoneNumber)
    {
        WriteToPhoneBook.addEntry(name, phoneNumber);
    }
    public void getPhoneBook() throws IOException, ParseException
    {
        ReadDataFromPhoneBook.readPhoneBook();
    }
    public void getPhoneNumberByName(String name) throws IOException, ParseException
    {
        boolean numberFoundFlag = ReadDataFromPhoneBook.getNumberByName(name);
        if (!numberFoundFlag)
        { System.out.println("The name wasn't found in the phone book"); }

    }

}

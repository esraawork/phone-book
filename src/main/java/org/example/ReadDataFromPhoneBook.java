package org.example;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.IOException;

public class ReadDataFromPhoneBook
{
    private static JSONObject jsonObject = new JSONObject();
    private static final JSONParser jsonParser = new JSONParser();
    public static boolean getNumberByName(String name) throws IOException, ParseException
    {
        FileReader jsonReader = new FileReader("phoneBookData.json");
        jsonObject = (JSONObject) jsonParser.parse(jsonReader);
        boolean numberFoundFlag = false;
        for (Object key : jsonObject.keySet())
        {
            if (key.toString().toLowerCase().contains(name.toLowerCase()))
            {
                System.out.println("name: " + key.toString() + ", phone number: " + jsonObject.get(key));
                numberFoundFlag = true;
            }
        }
        return numberFoundFlag;
    }
    public static void readPhoneBook() throws IOException, ParseException
    {
        FileReader jsonReader = new FileReader("phoneBookData.json");
        jsonObject = (JSONObject) jsonParser.parse(jsonReader);

        if (jsonObject.isEmpty())
        { System.out.println(" The phone book is empty, please add some entries first"); }
        else
        {
            System.out.println("The phones list is: ");
            for (Object key : jsonObject.keySet())
            { System.out.println("name: " + key.toString() + ", phone number: " + jsonObject.get(key)); }
        }
    }
}

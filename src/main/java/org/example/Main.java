package org.example;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) throws IOException, ParseException
    {
        PhoneBook phoneBookInstance = new PhoneBook();
        Scanner scannerObj = new Scanner(System.in);
        System.out.println("Hello this is your phone book application ");

        boolean continueRunning = true;
        while (continueRunning)
        { continueRunning = operationsOnPhoneBook(phoneBookInstance, scannerObj); }
    }
    public static boolean operationsOnPhoneBook(PhoneBook phoneBookInstance, Scanner scannerObj) throws IOException, ParseException
    {
        System.out.println("please choose one of the following operations: ");
        System.out.println("1. Add new phone number");
        System.out.println("2. List the current phone numbers");
        System.out.println("3. Search for a name");
        System.out.println("4. Exit program");
        int userOption = scannerObj.nextInt();
        boolean continueRunning = true;

        switch (userOption){
            case 1:
                System.out.println("Please enter the name: ");
                scannerObj.nextLine();
                String name = scannerObj.nextLine();
                System.out.println("Please enter the phone number: ");
                String number = scannerObj.nextLine();
                phoneBookInstance.addNewPhone(name, number);
                System.out.println("the phone number was added successfully");
                break;
            case 2:
                phoneBookInstance.getPhoneBook();
                break;
            case 3:
                System.out.println("Please enter the name: ");
                scannerObj.nextLine();
                String nameToSearchFor = scannerObj.nextLine();
                phoneBookInstance.getPhoneNumberByName(nameToSearchFor);
                break;
            case 4:
                continueRunning = false;
                break;
            default:
                System.out.println("Invalid Input");
        }
        return continueRunning;
    }
}